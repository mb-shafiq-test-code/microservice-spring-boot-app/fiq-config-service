package com.shafiq.fiqconfigservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class FiqConfigServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FiqConfigServiceApplication.class, args);
	}

}
